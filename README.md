# Lambda Shell

Deploy the function(s) using the Serverless framework. Note the endpoints.

I find it's easiest to connect to the websocket using wscat. After you've connected, the function accepts commands in the format:

```
{"action": "shell", "command": "..."}
```

I've included a patched version of wscat to strip away the boilerplate. Note that you should escape double quotes when using that.
